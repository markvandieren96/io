@echo off
cargo build
cargo build --features "async"
cargo build --features "compression"
cargo build --features "ron_format"
cargo build --features "json_format"
cargo build --features "bin_format"
cargo build --features "compression bin_format"
cargo build --features "async compression ron_format json_format bin_format"
pause