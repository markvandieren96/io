@echo off
cargo test
cargo test --features "async"
cargo test --features "compression"
cargo test --features "ron_format"
cargo test --features "json_format"
cargo test --features "bin_format"
cargo test --features "compression bin_format"
cargo test --features "async compression ron_format json_format bin_format"
pause