use crate::Error;
use std::{
	fs,
	path::{Path, PathBuf},
};

pub fn find_all_files(dir: impl AsRef<Path>, recursive: bool) -> Vec<PathBuf> {
	let mut path = dir.as_ref();
	if dir.as_ref() == Path::new("") {
		path = Path::new(".");
	}

	let mut paths = Vec::new();
	if let Ok(read_dir_it) = fs::read_dir(path) {
		for dir_entry in read_dir_it.flatten() {
			let path = dir_entry.path();
			if path.is_dir() {
				if recursive {
					paths.append(&mut find_all_files(&path, recursive));
				}
			} else {
				paths.push(path.clone());
			}
		}
	}
	paths
}

pub fn find_all_files_with_blacklist(
	dir: impl AsRef<Path>,
	recursive: bool,
	blacklist: &[&str],
) -> Vec<PathBuf> {
	let mut path = dir.as_ref();
	if dir.as_ref() == Path::new("") {
		path = Path::new(".");
	}

	let mut paths = Vec::new();
	if let Ok(read_dir_it) = fs::read_dir(path) {
		'outer: for dir_entry in read_dir_it.flatten() {
			let path = dir_entry.path();
			if path.is_dir() {
				for blacklist_item in blacklist.iter() {
					if path.to_string_lossy().contains(blacklist_item) {
						continue 'outer;
					}
				}
				if recursive {
					paths.append(&mut find_all_files_with_blacklist(
						&path, recursive, blacklist,
					));
				}
			} else {
				paths.push(path.clone());
			}
		}
	}
	paths
}

pub fn find_all_folders(dir: impl AsRef<Path>, recursive: bool) -> Vec<PathBuf> {
	let mut path = dir.as_ref();
	if dir.as_ref() == Path::new("") {
		path = Path::new(".");
	}

	let mut paths = Vec::new();
	if let Ok(read_dir_it) = fs::read_dir(path) {
		for dir_entry in read_dir_it.flatten() {
			let path = dir_entry.path();
			if path.is_dir() {
				paths.push(path.clone());
				if recursive {
					paths.append(&mut find_all_folders(&path, recursive));
				}
			}
		}
	}
	paths
}

pub fn make_dir(dir: impl AsRef<Path>) {
	let _ = fs::create_dir_all(dir);
}

pub fn exists(path: impl AsRef<Path>) -> bool {
	path.as_ref().exists()
}

pub fn set_working_dir<P: AsRef<Path>>(path: P) -> Result<(), Error> {
	std::env::set_current_dir(&path).map_err(|e| e.into())
}
