use crate::Error;
use std::path::Path;

pub fn load_from_ron<T>(path: impl AsRef<Path>) -> Result<T, Error>
where
	T: serde::de::DeserializeOwned,
{
	let text = crate::read_text(path)?;
	ron::de::from_str(&text).map_err(|e| e.into())
}

pub fn save_to_ron<T>(path: impl AsRef<Path>, obj: &T) -> Result<(), Error>
where
	T: serde::Serialize,
{
	let text = ron::ser::to_string_pretty(obj, ron::ser::PrettyConfig::default())?;
	crate::write_text(path, &text)
}

impl From<ron::error::Error> for Error {
	fn from(e: ron::error::Error) -> Error {
		Error(format!("{}", e))
	}
}

impl From<ron::de::SpannedError> for Error {
	fn from(e: ron::de::SpannedError) -> Error {
		Error(format!("{}", e))
	}
}
