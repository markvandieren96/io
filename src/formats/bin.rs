use crate::Error;
use std::path::Path;

pub fn load_from_binary<T>(path: impl AsRef<Path>) -> Result<T, Error>
where
	T: serde::de::DeserializeOwned,
{
	let bytes = crate::read_bytes(&path)?;
	bincode::deserialize(&bytes).map_err(|e| e.into())
}

pub fn save_to_binary<T>(path: impl AsRef<Path>, obj: &T) -> Result<(), Error>
where
	T: serde::Serialize,
{
	let bytes = bincode::serialize(obj)?;
	crate::write_bytes(path, &bytes)
}

impl From<Box<bincode::ErrorKind>> for Error {
	fn from(e: Box<bincode::ErrorKind>) -> Error {
		Error(format!("{}", e))
	}
}
