use crate::Error;
use std::path::Path;

pub fn load_from_json<T>(path: impl AsRef<Path>) -> Result<T, Error>
where
	T: serde::de::DeserializeOwned,
{
	let text = crate::read_text(path)?;
	serde_json::from_str(&text).map_err(|e| e.into())
}

pub fn save_to_json<T>(path: impl AsRef<Path>, obj: &T) -> Result<(), Error>
where
	T: serde::Serialize,
{
	let buf = Vec::new();
	let formatter = serde_json::ser::PrettyFormatter::with_indent(b"\t");
	let mut ser = serde_json::Serializer::with_formatter(buf, formatter);
	obj.serialize(&mut ser)?;
	let text = String::from_utf8(ser.into_inner())?;
	crate::write_text(path, &text)
}

impl From<serde_json::Error> for Error {
	fn from(e: serde_json::Error) -> Error {
		Error(format!("{}", e))
	}
}
