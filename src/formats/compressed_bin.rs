use crate::Error;
use bincode::serialize;
use flate2::{read::ZlibDecoder, write::ZlibEncoder, Compression};
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

pub fn load_from_compressed_binary<T>(path: impl AsRef<Path>) -> Result<T, Error>
where
	T: serde::de::DeserializeOwned,
{
	let compressed_bytes = crate::read_bytes(&path)?;
	let mut d = ZlibDecoder::new(&*compressed_bytes);
	let mut bytes = Vec::new();
	d.read_to_end(&mut bytes)?;
	bincode::deserialize(&bytes).map_err(|e| e.into())
}

pub fn save_to_compressed_binary<T>(path: impl AsRef<Path>, obj: &T) -> Result<(), Error>
where
	T: serde::Serialize,
{
	let bytes = serialize(obj)?;
	let mut e = ZlibEncoder::new(Vec::new(), Compression::default());
	e.write_all(&*bytes)?;
	let compressed = e.finish()?;
	crate::write_bytes(path, &compressed)
}

pub fn read_bytes_compressed(path: impl AsRef<Path>) -> Result<Vec<u8>, Error> {
	let mut f = File::open(path.as_ref()).map_err(|e| format!("{} (path={})", e, path.as_ref().display()))?;
	let mut buffer = Vec::new();
	f.read_to_end(&mut buffer)?;
	decompress(&buffer)
}

pub fn write_bytes_compressed(path: impl AsRef<Path>, bytes: &[u8]) -> Result<(), Error> {
	let mut file = File::create(&path)?;
	let compressed = compress(bytes)?;
	file.write_all(&compressed).map_err(|e| e.into())
}

pub fn compress(data: &[u8]) -> Result<Vec<u8>, Error> {
	let mut e = ZlibEncoder::new(Vec::new(), Compression::default());
	e.write_all(data)?;
	Ok(e.finish()?)
}

pub fn decompress(data: &[u8]) -> Result<Vec<u8>, Error> {
	let mut d = ZlibDecoder::new(data);
	let mut buffer = Vec::new();
	d.read_to_end(&mut buffer)?;
	Ok(buffer)
}

#[cfg(test)]
mod tests {
	use super::*;

	const TEST_DATA: &[u8] = &[0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2];

	#[test]
	fn test_compress() {
		let compressed = compress(TEST_DATA).unwrap();
		let decompressed = decompress(&compressed).unwrap();
		assert_eq!(TEST_DATA, decompressed.as_slice());
	}

	#[test]
	fn test_compress_with_file() {
		write_bytes_compressed("test.cbin", TEST_DATA).unwrap();
		let decompressed = read_bytes_compressed("test.cbin").unwrap();
		assert_eq!(TEST_DATA, decompressed.as_slice());
	}
}
