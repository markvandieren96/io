pub struct Error(pub String);

impl From<std::io::Error> for Error {
	fn from(e: std::io::Error) -> Error {
		Error(format!("{}", e))
	}
}

impl From<std::string::FromUtf8Error> for Error {
	fn from(e: std::string::FromUtf8Error) -> Error {
		Error(format!("{}", e))
	}
}

impl From<String> for Error {
	fn from(e: String) -> Error {
		Error(e)
	}
}

impl From<Error> for String {
	fn from(e: Error) -> String {
		format!("{}", e)
	}
}

impl std::fmt::Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.0)
	}
}

impl std::fmt::Debug for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{:?}", self.0)
	}
}

impl std::error::Error for Error {}
