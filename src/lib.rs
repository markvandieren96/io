mod dir;
mod error;
mod file;

mod formats {
	#[cfg(feature = "bin_format")]
	mod bin;

	#[cfg(feature = "bin_format")]
	pub use bin::*;

	#[cfg(all(feature = "compression", feature = "bin_format"))]
	mod compressed_bin;

	#[cfg(all(feature = "compression", feature = "bin_format"))]
	pub use compressed_bin::*;

	#[cfg(feature = "json_format")]
	mod json;

	#[cfg(feature = "json_format")]
	pub use json::*;

	#[cfg(feature = "ron_format")]
	mod ron;

	#[cfg(feature = "ron_format")]
	pub use self::ron::*;
}

#[cfg(feature = "async")]
pub mod file_async;

#[cfg(feature = "async")]
pub mod dir_async;

pub mod prelude {
	#[cfg(feature = "async")]
	pub use crate::file_async;

	#[cfg(feature = "async")]
	pub use crate::dir_async;

	pub use crate::dir::*;
	pub use crate::error::*;
	pub use crate::file::*;

	pub use crate::formats::*;
}

pub use prelude::*;
