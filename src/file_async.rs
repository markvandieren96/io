use crate::prelude::Error;
use async_std::io::prelude::{ReadExt, WriteExt};
use async_std::{fs::File, path::Path};

pub async fn read_bytes(path: impl AsRef<Path>) -> Result<Vec<u8>, Error> {
	let mut f = File::open(path.as_ref()).await?;
	let mut buffer = Vec::new();
	f.read_to_end(&mut buffer).await?;
	Ok(buffer)
}

pub async fn write_bytes(path: impl AsRef<Path>, bytes: &[u8]) -> Result<(), Error> {
	let mut file = File::create(&path).await?;
	file.write_all(bytes).await.map_err(|e| e.into())
}

#[cfg(feature = "bin_format")]
pub async fn load_from_binary<T>(path: impl AsRef<Path>) -> Result<T, Error>
where
	T: serde::de::DeserializeOwned,
{
	let bytes = read_bytes(&path).await?;
	bincode::deserialize(&bytes).map_err(|e| e.into())
}

#[cfg(all(feature = "compression", feature = "bin_format"))]
mod compression {
	use super::*;
	use bincode::serialize;
	use flate2::{read::ZlibDecoder, write::ZlibEncoder, Compression};

	pub async fn load_from_compressed_binary<T>(path: impl AsRef<Path>) -> Result<T, Error>
	where
		T: serde::de::DeserializeOwned,
	{
		let compressed_bytes = read_bytes(&path).await?;
		let mut d = ZlibDecoder::new(&*compressed_bytes);
		let mut bytes = Vec::new();
		use std::io::Read;
		d.read_to_end(&mut bytes)?;
		bincode::deserialize(&bytes).map_err(|e| e.into())
	}
	pub async fn save_to_compressed_binary<T>(path: impl AsRef<Path>, obj: &T) -> Result<(), Error>
	where
		T: serde::Serialize,
	{
		let bytes = serialize(obj)?;
		let mut e = ZlibEncoder::new(Vec::new(), Compression::default());
		use std::io::Write;
		e.write_all(&*bytes)?;
		let compressed = e.finish()?;
		write_bytes(path, &compressed).await
	}
}

#[cfg(all(feature = "compression", feature = "bin_format"))]
pub use compression::*;
