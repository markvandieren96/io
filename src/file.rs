use crate::prelude::Error;
use std::fs;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

pub fn read_text(path: impl AsRef<Path>) -> Result<String, Error> {
	fs::read_to_string(&path).map_err(|e| format!("{} (path={})", e, path.as_ref().display()).into())
}

pub fn write_text(path: impl AsRef<Path>, text: &str) -> Result<(), Error> {
	let mut file = File::create(&path)?;
	file.write_all(text.as_bytes()).map_err(|e| e.into())
}

pub fn read_bytes(path: impl AsRef<Path>) -> Result<Vec<u8>, Error> {
	let mut f = File::open(path.as_ref()).map_err(|e| format!("{} (path={})", e, path.as_ref().display()))?;
	let mut buffer = Vec::new();
	f.read_to_end(&mut buffer)?;
	Ok(buffer)
}

pub fn write_bytes(path: impl AsRef<Path>, bytes: &[u8]) -> Result<(), Error> {
	let mut file = File::create(&path)?;
	file.write_all(bytes).map_err(|e| e.into())
}
