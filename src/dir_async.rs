use async_std::path::Path;

pub async fn exists(path: impl AsRef<Path>) -> bool {
	path.as_ref().exists().await
}
